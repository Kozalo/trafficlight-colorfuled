Traffic light & colorful LED
============================

This is a simple project for **Arduino Nano**, based on ATmega 328/P, written in plain C.

![What it looks like](http://kozalo.ru/images/posts/1899161006.jpeg)

[Video demonstration on Instagram.](https://www.instagram.com/p/BdMEqyqAUcd/)


Requirements
------------

Besides the microcontroller, to reproduce the project by yourself you need:

- red, green and yellow LEDs;
- RGB LED;
- 6 resistors (1 kΩ in my case but it's not a strict requirement; use any online calculator to get minimal values for
each color);
- wires, of course.

![Circuit](http://kozalo.ru/images/posts/535583219.png)

![Circuit on breadboard](http://kozalo.ru/images/posts/1251149273.jpeg)


Building and uploading
----------------------

### Windows

I would recommend to write code and build the project in AtmelStudio — the official IDE, based on Visual Studio, from
the manufacturer of the chip. It contains all necessary libraries, a compiler, and linker. However, I didn't figure out
how to upload a firmware here. Therefore, I use SinaProg for that instead. In fact, this small program is just a GUI
wrapper over AVRDude.

![AVRDude](http://kozalo.ru/images/posts/756143763.png)


### Linux (Debian 8)

First of all, you need to install a bunch of packages: **gcc-avr** (a compiler), **avr-libc** (a standard library
of the C language), **binutils-avr** (some utilities such as a linker and converter of different binary formats),
**avrdude** (a program to upload your firmware into the microcontroller).

To compile the project, type the following command:

```bash
avr-gcc -Os -std=c99 -mmcu=atmega328p -o firmware.elf main.c initialization.c trafficlight.c
```

Then you need to convert the produced ELF file into a HEX file:

```bash
avr-objcopy -j .text -j .data -O ihex firmware.elf firmware.hex
```

After that, it's possible to upload the firmware into the microcontroller:

```bash
# It may be required to replace ttyUSB0 with your actual serial port.
avrdude -pm328p -carduino -P/dev/ttyUSB0 -b57600 -Uflash:w:firmware.hex:i
```

In fact, if you clone the project from the repository, you'll get two ready-to-use shell scripts: **build.sh** and
**upload.sh**. The latter runs the build process automatically if it cannot find a firmware file. So, just issue this
command:

```bash
./upload.sh USB0    # or whatever serial port your Arduino is connected to
```

![Building and uploading on Debian 8](http://kozalo.ru/images/posts/1416457801.png)
