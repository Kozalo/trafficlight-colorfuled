#!/bin/sh

cd PWM

if [ ! -d build ]; then
    mkdir build
fi

echo Building the project...

avr-gcc -Os -std=c99 -mmcu=atmega328p -o build/firmware.elf main.c initialization.c trafficlight.c
avr-objcopy -j .text -j .data -O ihex build/firmware.elf build/firmware.hex

cd ..
echo The build has been finished.
