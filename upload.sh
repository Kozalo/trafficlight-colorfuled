#!/bin/sh

if [ ! -f PWM/build/firmware.hex ]; then
    ./build.sh
fi

# Usually, the script takes values such as "USB0", but it's also tolerant to arguments like "ttyUSB0" or "/dev/ttyUSB0".
ARG="${1:-USB0}"
DEVID="${ARG#*tty}"
DEVPATH="/dev/tty$DEVID"

echo "Trying to upload the firmware to $DEVPATH..."
avrdude -pm328p -carduino -P"$DEVPATH" -b57600 -Uflash:w:PWM/build/firmware.hex:i
