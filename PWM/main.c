#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <stdint.h>

#include "pins.h"
#include "trafficlight.h"
#include "initialization.h"


// Number of blinks of the green light before it will be replaced by the yellow one
// Must be an odd number!
#define BLINKS 7

#if BLINKS % 2 == 0
	#error "BLINKS must be an odd number!"
#endif


volatile uint8_t red = 0;
volatile uint8_t green = 0;
volatile uint8_t blue = 0;


int main(void)
{	
	initializeRgbLed();
	initializeTraficLight();
	// Enable global interruptions
	sei();
	// Put the device into a slightest sleep mode
	set_sleep_mode(SLEEP_MODE_IDLE);
	while (1) {
		sleep_mode();
	}
}


// Handler of RgbLed's change of color
ISR(TIMER0_OVF_vect) {
	// Increase the red component (and decrease the blue one if it's not the first iteration)
	if (red < 0xFF && green == 0)
	{
		if (blue > 0) {
			BLUE_COMPONENT = --blue;
		}
		RED_COMPONENT = ++red;
	}
	// Increase the green component and decrease the red one
	else if (green < 0xFF && blue == 0)
	{
		RED_COMPONENT = --red;
		GREEN_COMPONENT = ++green;
	}
	// Increases the blue component and decrease the green one
	else if (blue < 0xFF)
	{
		GREEN_COMPONENT = --green;
		BLUE_COMPONENT = ++blue;
	}
}

// Handler of TrafficLight's change of state
ISR(TIMER1_OVF_vect) {
	// If the red is currently lighting...
	if (trafficLightState.color.red) {
		// ...and the yellow is still not, they both must light
		if (!trafficLightState.color.yellow) {
			turnYellowOn();
		}
		// If they both are lighting, let's move on to the green light
		else {
			turnRedOff();
			turnYellowOff();
			turnGreenOn();
		}
	}
	// The yellow light without the red one can be only after the green
	else if (trafficLightState.color.yellow) {
		turnYellowOff();
		turnRedOn();
	}
	// We've already checked all other options and definitely know that only the green can be lighting now. Or not, if it's blinking
	else {
		// If it's not blinking, let's make it do that!
		if (!trafficLightState.isBlinking) {
			turnGreenOff();
			trafficLightState.blinksToChange = BLINKS;
			trafficLightState.isBlinking = true;
			// Blinks should be faster than usual color replacement
			setFastTimer();
		}
		// Blinks implementation
		else if (trafficLightState.blinksToChange > 0) {
			if (trafficLightState.blinksToChange % 2 == 0) {
				turnGreenOn();
			} else {
				turnGreenOff();
			}
			trafficLightState.blinksToChange--;
		}
		// When the time has come, let's move on to the yellow light
		else {
			turnGreenOff();
			turnYellowOn();
			trafficLightState.isBlinking = false;
			// Reset the speed of the timer
			setSlowTimer();
		}
	}
	
}
