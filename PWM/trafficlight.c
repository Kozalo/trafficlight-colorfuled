#include <avr/io.h>
#include <stdbool.h>

#include "pins.h"
#include "trafficlight.h"


void turnRedOn(void) {
	TRAFFIC_LIGHT_PORT |= _BV(RED_LIGHT);
	trafficLightState.color.red = true;
}

void turnRedOff(void) {
	TRAFFIC_LIGHT_PORT &= ~_BV(RED_LIGHT);
	trafficLightState.color.red = false;
}

void turnYellowOn(void) {
	TRAFFIC_LIGHT_PORT |= _BV(YELLOW_LIGHT);
	trafficLightState.color.yellow = true;
}

void turnYellowOff(void) {
	TRAFFIC_LIGHT_PORT &= ~_BV(YELLOW_LIGHT);
	trafficLightState.color.yellow = false;
}

void turnGreenOn(void) {
	TRAFFIC_LIGHT_PORT |= _BV(GREEN_LIGHT);
	trafficLightState.color.green = true;
}

void turnGreenOff(void) {
	TRAFFIC_LIGHT_PORT &= ~_BV(GREEN_LIGHT);
	trafficLightState.color.green = false;
}


void setFastTimer(void) {
	TCCR1B |= _BV(CS11) | _BV(CS10);
	TCCR1B &= ~_BV(CS12);
}

void setSlowTimer(void) {
	TCCR1B |= _BV(CS12) | _BV(CS10);
	TCCR1B &= ~_BV(CS11);
}
