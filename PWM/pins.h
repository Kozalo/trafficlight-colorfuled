#ifndef PINS_H_
#define PINS_H_

// ** RGB LED **

#define RED_PIN			PD6			// D6
#define RED_PIN_PORT	PORTD
#define RED_PIN_DDR		DDRD

#define GREEN_PIN		PD3			// D3
#define GREEN_PIN_PORT	PORTD
#define GREEN_PIN_DDR	DDRD

#define BLUE_PIN		PB3			// D11
#define BLUE_PIN_PORT	PORTB
#define BLUE_PIN_DDR	DDRB


// Values will be written to these registers to utilize hardware PWM
#define RED_COMPONENT   OCR0A		// D6
#define GREEN_COMPONENT OCR2B		// D3
#define BLUE_COMPONENT  OCR2A		// D11


// ** TRAFFIC LIGHT **

#define TRFFIC_LIGHT_DDR	DDRC
#define TRAFFIC_LIGHT_PORT  PORTC
#define RED_LIGHT			PC2		// A2
#define YELLOW_LIGHT		PC1		// A1
#define GREEN_LIGHT			PC0		// A0

#endif