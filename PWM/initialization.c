#include <avr/io.h>

#include "pins.h"
#include "trafficlight.h"
#include "initialization.h"


void initializeRgbLed(void) {
	// Set pins to OUTPUT
	RED_PIN_DDR |= _BV(RED_PIN);
	GREEN_PIN_DDR |= _BV(GREEN_PIN);
	BLUE_PIN_DDR |= _BV(BLUE_PIN);
	
	// Set its values to zero
	RED_PIN_PORT &= ~_BV(RED_PIN);
	GREEN_PIN_PORT &= ~_BV(GREEN_PIN);
	BLUE_PIN_PORT &= ~_BV(BLUE_PIN);

	// Reset Timer/Counter0 (D6 and D5) and Timer/Counter2 (D11, D3)
	TCNT0 = 0;
	TCNT2 = 0;

	// Set initial values to zero
	RED_COMPONENT = 0;
	GREEN_COMPONENT = 0;
	BLUE_COMPONENT = 0;

	// Fast PWM, clear 0CR0A at Compare Match, set at BOTTOM (non-inverting mode)
	TCCR0A |= _BV(COM0A1) | _BV(WGM01) | _BV(WGM00);
	// Prescaler = 1024
	TCCR0B |= _BV(CS02) | _BV(CS00);

	// Ditto, but OCR2A (OC2A at pin) and OCR2B (OC2B)
	TCCR2A |= _BV(COM2A1) | _BV(COM2B1) | _BV(WGM21) | _BV(WGM20);	
	TCCR2B |= _BV(CS22) | _BV(CS20);

	// Enable the Overflow Interrupt
	TIMSK0 |= _BV(TOIE0);	
}

void initializeTraficLight(void) {
	// Set pins to OUTPUT
	TRFFIC_LIGHT_DDR |= _BV(RED_LIGHT) | _BV(YELLOW_LIGHT) | _BV(GREEN_LIGHT);

	// Set initial values
	turnRedOn();
	turnYellowOff();
	turnGreenOff();

	// Reset Timer/Counter1. In this program we're utilizing it only for interruptions (i.e. in the normal mode).
	TCNT1 = 0x00;
	// Prescaler = 1024
	setSlowTimer();

	// Enable the Overflow Interrupt
	TIMSK1 |= _BV(TOIE1);
}
