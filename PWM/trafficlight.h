#ifndef TRAFFICLIGHT_H_
#define TRAFFICLIGHT_H_

#include <stdbool.h>


struct TrafficLightColor {
	bool red;
	bool yellow;
	bool green;
};

struct TrafficLightState {
	struct TrafficLightColor color;
	bool isBlinking;
	unsigned int blinksToChange;
} trafficLightState;


void turnRedOn(void);
void turnRedOff(void);
void turnYellowOn(void);
void turnYellowOff(void);
void turnGreenOn(void);
void turnGreenOff(void);

void setFastTimer(void);
void setSlowTimer(void);

#endif